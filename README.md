roomfinder
---

Used for finding empty rooms for group work/studying (or hooking up to the projector and playing a movie/game, you do you)

## Usage
1. Make sure you have python3 installed, this won't work with python 2 or lower
2. If the current semester's data isn't in the data folder, download it [here](https://navigator.cnu.edu/StudentScheduleofClasses/). (further instructions in the data/ directory)
3. Run the script with python3, you shouldn't need to install any extra packages as all of the ones that it uses are installed by default

#### Runtime instructions:
When prompted:

1. Enter the index corresponding to the file name (they are zero-indexed and shown to the left of the file name)
2. Enter the building code, it pulls these out of the .csv so it doesn't show the proper names, but the main campus building codes are as follows:
    |Code   |Building name |
    |-------|--------------|
    |LUTR   |Luter Hall    |
    |MCM    |McMurran Hall |
    |FORBES |Forbes Hall   |
    |TRIB   |Trible Library|
3. Enter what day of the week you are looking at (single letter, one of M, T, W, H, F like it shows in the catalog)
4. Enter the time (24h format with no :, e.g. 900 for 9:00 AM or 1500 for 3:00 PM)
5. It should now display all of the rooms that are currently open in the selected building, as well as the time that the next class in that room starts, or that there aren't any more classes for the day there.
6. If you want to see a complete time schedule for all of the rooms in the building, enter "y" when prompted after the rooms are first shown, otherwise enter "n" to finish the program. (note: if no time slots are shown when you enter "y" that means that there are no classes in that room on the selected day)

#### Extra notes on operation:
All inputs are case-insensitive, for example for the building code "LUTR", "lutr", and "lUtR" are all valid

Right now the time input only takes 24h format, eventually I'm planning on updating it so that if the time has a ":" and an AM/PM then it'll autoconvert but for now just cry about it (or just take the time and add 12 to the hour if it's after noon)

Since it is getting the data from the course schedule .csv file, it should never show professor's offices but will also not show any rooms that don't have classes for that semester, so if there aren't any open roomes according to this script you might want to just go and check (although the upcoming timefinder and classfinder scripts should be able help to determine if a room has no classes)

##### DISCLAIMER:
All scripts provided in this project are intended for reference only, and by using it you agree that I am not responsible for:

1. Anything that you do with the information (i.e. trespassing or using the classrooms for something they shouldn't be)
2. You not being able to find a place to work/study (there are lots of places that aren't listed in here, check 'em)
3. Any classes not being where they should be or being where they shouldn't be due to classroom or schedule changes not listed in the course catalog
5. Any awkward encounters that you may have while searching for a room (like another group already working there)
6. Any other classes or groups walking in on you/your group
7. Any of the above caused by errors in the script, it is by no means error-free so if you have doubts just check it yourself

The only way to be sure that a room is taken or empty is to check it yourself, this script just provides a starting place!

##### Licence:
This program is licensed under the WTFPL-2.0 License and the Unlicense


For details about what these mean, check out these links:

* [WTFPL-2.0](https://tldrlegal.com/license/do-wtf-you-want-to-public-license-v2-(wtfpl-2.0))
* [Unlicense](https://tldrlegal.com/license/unlicense)

WTFPL-2.0 Full Text:
```
        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
                    Version 2, December 2004 

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 

 Everyone is permitted to copy and distribute verbatim or modified 
 copies of this license document, and changing it is allowed as long 
 as the name is changed. 

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

  0. You just DO WHAT THE FUCK YOU WANT TO.
```

Unlicense Full Text:
```
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
```
