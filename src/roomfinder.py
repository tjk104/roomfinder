"""
roomfinder.py
used for finding an empty classroom given a building and a time
usage: just run roomfinder.py (using python 3), and make sure that
the current semester's .csv is in the data folder (obtainable from
the CNU Schedule of Classes website)
"""
import sys
import os
import csv

file_options = os.listdir('../data/')

print('\nWhich file would you like to use?')
for i, path in enumerate(file_options):
    print('\t{}: {}'.format(i, path))

file_index = -1
while file_index not in [str(n) for n in range(len(file_options))]:
    file_index = input('Enter the file number: ')
file_index = int(file_index)

file_name = '../data/' + file_options[file_index]

sys.stdout.write("Parsing classes...")
data_list = []

with open(file_name) as data_file:
    csv_file = csv.reader(data_file)
    for row in csv_file:
        data_list.append(row)

sys.stdout.write(" Done!\n")

sys.stdout.write("Cleaning data...")
# used fields (in order): course (SUBJ 000), days (MTWHF), time (military time range), room (BLDG 000)
fields = [1,7, 8, 9]
course_list = [[row[i] for i in fields] for row in data_list[1:]]
sys.stdout.write(" Done!\n")
sys.stdout.write("Getting building ids...")
buildings_list = [row[3].split(' ')[0] for row in course_list]
buildings_list = list(set(buildings_list))
buildings_list.remove('ARR')
buildings_list.sort()
sys.stdout.write(" Done!\n")

print("\nWhat building are you looking at?")

print(' '.join(buildings_list))

building = ''
while building not in buildings_list:
    building = input("Enter the building code: ").upper()

sys.stdout.write('Searching building...')
possible_courses = [course for course in course_list if (building in course[3])]

for i, course in enumerate(possible_courses):
    if ';' in ' '.join(course[1:]):
        new_course = [course[0]]
        for n in range(1,4):
            if ';' in course[n]:
                new_course.append(course[n].split(';')[1].strip())
                possible_courses[i][n] = course[n].split(';')[0].strip()
            else:
                new_course.append(course[n])
        possible_courses.append(new_course)

sys.stdout.write(' Done!\n')

day = ''
while day not in ['M', 'T', 'W', 'H', 'F']:
    day = input('\nWhat day of the week? (MTWHF): ').upper()

sys.stdout.write('Finding class times...')

day_courses = [course for course in possible_courses if day in course[1]]
day_rooms = list(set([course[3] for course in day_courses]))
possible_rooms = list(set([course[3] for course in possible_courses]))

room_times = {}
for room in possible_rooms:
    room_times[room] = [course[2] for course in day_courses if (room in course[3]) and (day in course[1])]
sys.stdout.write(' Done!\n')

time = -1
while not 0 <= time <= 2400:
    time = input('\nWhat time? (in 24h time, no ":"): ')
    if time.isdigit():
        time = int(time)

sys.stdout.write('Searching for available times...')

available_rooms = day_rooms[:]
for room, times in room_times.items():
    if len(times) == 0:
        continue
    for slot in times:
        if int(slot.split('-')[0]) <= time <= int(slot.split('-')[1]):
            if room in available_rooms:
                available_rooms.remove(room)
                possible_rooms.remove(room)

available_rooms = list(set(available_rooms + possible_rooms))


sys.stdout.write(' Done!\n')
print('')

available_rooms.sort()

if len(available_rooms) > 0:
    for room in available_rooms:
        if len(room_times[room]) == 0:
            print('{}: No classes on that day!'.format(room))
        else:
            start_times = [int(n.split('-')[0].strip()) for n in room_times[room]]
            start_times = sorted([n for n in start_times if n > time])
            if len(start_times) > 0:
                next_time = start_times[0]
                print('{}: Next class starts at {:03d}'.format(room, next_time))
            else:
                print('{}: No classes after {:03d}!'.format(room, time))
    print('')
    if input('View all class time slots for rooms? (Y/N): ').upper() == 'Y':
        print('')
        for room in available_rooms:
            times_list = room_times[room]
            times_list.sort()
            print('{}:\n\t{}'.format(room, '\n\t'.join(times_list)))
else:
    print("No rooms found!")
